import React, {useContext} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {AuthContext} from './app/components/AuthProviders';
import AppNavigator from './app/navigator/AppNavigator';
import {SafeAreaView} from 'react-native-safe-area-context';
import AuthNavigator from './app/navigator/AuthNavigator';
import {useColorScheme} from 'react-native';
import NavigationTheme from './app/navigator/NavigationTheme';

export const Routes = () => {
  const {isLoginedIn} = useContext(AuthContext);
  const scheme = useColorScheme();
  console.log(scheme);
  return (
    <SafeAreaView style={{flex: 1}}>
      <NavigationContainer
        theme={scheme === 'dark' ? NavigationTheme : NavigationTheme}>
        {isLoginedIn ? <AppNavigator /> : <AuthNavigator />}
      </NavigationContainer>
    </SafeAreaView>
  );
};
