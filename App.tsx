import 'react-native-gesture-handler';
import React from 'react';
import {StatusBar, View} from 'react-native';
import colors from './app/config/colors';
import {AuthProviders} from './app/components/AuthProviders';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {Routes} from './Routes';

const App = () => {
  return (
    <SafeAreaProvider>
      <View style={{flex: 1}}>
        <AuthProviders>
          <StatusBar
            animated={true}
            backgroundColor={colors.white}
            barStyle="dark-content"
            showHideTransition="slide"
          />
          <Routes />
        </AuthProviders>
      </View>
    </SafeAreaProvider>
  );
};

export default App;
