export interface Category {
  backgroundColor: string;
  icon: string;
  label: string;
  value: number;
}
