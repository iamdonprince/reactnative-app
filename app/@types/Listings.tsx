export interface ICard {
  title: string;
  subTitle: string;
  images: any;
  onPress?: any;
  price?: string;
  id?: string;
  imageUrl?: string;
}
