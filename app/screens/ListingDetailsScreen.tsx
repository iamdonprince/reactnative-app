import React from 'react';
import {View} from 'react-native';
import Card from '../components/Card';
import colors from '../config/colors';

export default function ListingDetailsScreen({route}: any) {
  const data = route.params;
  return (
    <View
      // eslint-disable-next-line react-native/no-inline-styles
      style={{
        padding: 15,
        backgroundColor: colors.lightGrey,
        flex: 1,
      }}>
      <Card
        title={data?.item.title}
        subTitle={'$' + data.item.price}
        image={data.item.image}
      />
    </View>
  );
}
