import React, {useState} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {Face, RNCamera} from 'react-native-camera';
import Ionicons from 'react-native-vector-icons/Ionicons';

const PendingView = () => (
  <View
    // eslint-disable-next-line react-native/no-inline-styles
    style={{
      flex: 1,
      backgroundColor: 'lightgreen',
      justifyContent: 'center',
      alignItems: 'center',
    }}>
    <Text>Waiting</Text>
  </View>
);

const ReactCamera = () => {
  const [cameraMode, setCameraMode] = useState(false);
  const [faceDetected, setFaceDetected] = useState<Face[]>();
  const [searchWaiting, setsearchWaiting] = useState<NodeJS.Timeout | null>(
    null,
  );
  // const [faceavl, setFaceavl] = useState(false);

  //   const [cameraRef, setCameraRef] = useState<any>();
  //   const [capturedImage, setCapturedImage] = useState<string>();
  const takePicture = async (camera: any) => {
    const options = {quality: 0.5, base64: true, pauseAfterCapture: true};
    const data = await camera.takePictureAsync(options);
    // setCapturedImage(data.uri);
    //  eslint-disable-next-line
    console.log(data);
  };
  console.log(faceDetected);
  const faceDetecedHandler = (faces: Face[]) => {
    setFaceDetected(faces);

    clearTimeout(searchWaiting!);
    let avc = setTimeout(() => {
      setFaceDetected([]);
    }, 500);
    setsearchWaiting(avc);
  };
  return (
    <View style={styles.container}>
      <RNCamera
        style={styles.preview}
        type={RNCamera.Constants.Type[cameraMode ? 'back' : 'front']}
        flashMode={RNCamera.Constants.FlashMode.auto}
        autoFocus={RNCamera.Constants.AutoFocus.on}
        focusDepth={1}
        onBarCodeRead={event => console.log(event)}
        onFaceDetectionError={event => console.log(event)}
        faceDetectionMode={RNCamera.Constants.FaceDetection.Mode.accurate}
        onFacesDetected={event => faceDetecedHandler(event.faces)}
        faceDetectionLandmarks={
          RNCamera.Constants.FaceDetection.Classifications.all
        }
        faceDetectionClassifications={
          RNCamera.Constants.FaceDetection.Classifications.all
        }
        androidCameraPermissionOptions={{
          title: 'Permission to use camera',
          message: 'We need your permission to use your camera',
          buttonPositive: 'Ok',
          buttonNegative: 'Cancel',
        }}
        androidRecordAudioPermissionOptions={{
          title: 'Permission to use audio recording',
          message: 'We need your permission to use your audio',
          buttonPositive: 'Ok',
          buttonNegative: 'Cancel',
        }}>
        {({camera, status}) => {
          if (status !== 'READY') return <PendingView />;
          //   setCameraRef(camera);
          return (
            <View
              // eslint-disable-next-line react-native/no-inline-styles
              style={styles.elemContainer}>
              {faceDetected && (
                <View
                  // eslint-disable-next-line react-native/no-inline-styles
                  style={{
                    width: faceDetected[0]?.bounds.size.width,
                    height: faceDetected[0]?.bounds.size.height,
                    borderWidth: 2,
                    borderColor: 'white',
                    backgroundColor: 'transparent',
                    position: 'absolute',
                    left: faceDetected[0]?.bounds.origin.x,
                    top: faceDetected[0]?.bounds.origin.y,
                  }}
                />
              )}
              <TouchableOpacity
                onPress={() => takePicture(camera)}
                style={styles.capture}>
                <Ionicons name="camera-outline" size={35} color="black" />
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.cameraMode}
                onPress={() => setCameraMode(!cameraMode)}>
                <Ionicons name="camera-reverse" size={35} color="white" />
              </TouchableOpacity>
            </View>
          );
        }}
      </RNCamera>
      {/* {capturedImage && (
        <Image
          style={{width: 300, height: 300}}
          source={{uri: capturedImage}}
        />
      )} */}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    backgroundColor: 'black',
  },
  preview: {
    width: '100%',
    height: '100%',
  },
  elemContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
  cameraMode: {
    position: 'absolute',
    bottom: 45,
    right: 35,
  },
  capture: {
    flex: 0,
    width: 70,
    height: 70,
    backgroundColor: '#fff',
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 40,
  },
});

export default ReactCamera;
