import React, {useContext, useState} from 'react';
import {useNavigation} from '@react-navigation/core';
import {FlatList, StyleSheet, View} from 'react-native';
import {AuthContext} from '../components/AuthProviders';
import Icon from '../components/Icon';
import ListItem from '../components/ListItem';
import colors from '../config/colors';

export default function AccountScreen() {
  const navigation = useNavigation();

  const {logout} = useContext(AuthContext);
  const [menuItem] = useState([
    {
      title: 'My Listing',
      icon: {
        name: 'format-list-bulleted',
        backgroundColor: colors.primary,
      },
    },
    {
      title: 'My Messages',
      onPress: () => navigation.navigate('Message'),

      icon: {
        name: 'email',
        backgroundColor: colors.secondary,
      },
    },
  ]);

  const userDetails = [
    {
      title: 'prince kumar',
      subTitle: 'princekumar@gmail.com',
      image: require('../assets/prince.jpg'),
    },
  ];

  const logoutBtn = [
    {
      title: 'Log Out',
      onPress: () => logout(),
      icon: {
        name: 'logout',
        backgroundColor: colors.yellow,
      },
    },
  ];

  return (
    <View style={styles.accountScreen}>
      <View style={styles.profile}>
        <FlatList
          data={userDetails}
          renderItem={({item}) => (
            <ListItem
              title={item.title}
              subTitle={item?.subTitle}
              image={item?.image}
            />
          )}
          keyExtractor={list => list.title}
        />
      </View>
      <View style={styles.listing}>
        <FlatList
          data={menuItem}
          renderItem={({item}) => (
            <ListItem
              title={item.title}
              onPress={item?.onPress}
              IconComponent={
                item?.icon && (
                  <Icon
                    name={item.icon.name}
                    backgroundColor={item.icon.backgroundColor}
                  />
                )
              }
            />
          )}
          keyExtractor={list => list.title}
        />
      </View>
      <View style={styles.logout}>
        <FlatList
          data={logoutBtn}
          renderItem={({item}) => (
            <ListItem
              title={item.title}
              onPress={item?.onPress}
              IconComponent={
                item?.icon && (
                  <Icon
                    name={item.icon.name}
                    backgroundColor={item.icon.backgroundColor}
                  />
                )
              }
            />
          )}
          keyExtractor={list => list.title}
        />
        {/* <ListItem
          onPress={() => logout()}
          title="Log Out"
          IconComponent={<Icon name="logout" backgroundColor={} />}
        /> */}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  accountScreen: {
    backgroundColor: colors.lightGrey,
    flex: 1,
  },
  profile: {
    marginTop: 20,
    marginBottom: 40,
    backgroundColor: colors.white,
  },
  listing: {
    marginBottom: 30,
    backgroundColor: colors.white,
  },
  logout: {
    marginBottom: 20,
    backgroundColor: colors.white,
  },
});
