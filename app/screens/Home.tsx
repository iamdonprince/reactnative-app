import React, {useEffect, useState} from 'react';
import {FlatList, StyleSheet, View} from 'react-native';
import Card from '../components/Card';
import colors from '../config/colors';
import routes from '../navigator/routes';
import listingsApi from '../api/listings';
import {ICard} from '../@types/Listings';
import AppText from '../components/AppText';
import AppButton from '../components/AppButton';
import ActivityIndicator from '../components/ActivityIndicator';

export default function Home({navigation}: any) {
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(false);
  const [listings, setListings] = useState<ICard[]>();
  const getListngsData = async () => {
    setIsLoading(!isLoading);
    try {
      const data = await listingsApi.getListings();
      setListings(data);
      setError(false);
      setIsLoading(!isLoading);
      console.log(data);
    } catch (err) {
      console.log(err, 'not working');
      setError(true);
    }
    setIsLoading(false);
  };
  useEffect(() => {
    getListngsData();
  }, []);
  return (
    <View style={styles.container}>
      <ActivityIndicator isVisible={isLoading} />
      <FlatList
        data={listings}
        renderItem={({item}) => (
          <Card
            title={item.title}
            subTitle={'$' + item.price}
            imageUrl={item.images[0].url}
            onPress={() => navigation.navigate(routes.LISTING_DETAILS, {item})}
          />
        )}
        keyExtractor={list => list.id?.toString() || list.title}
      />
      {error && (
        <View
          // eslint-disable-next-line react-native/no-inline-styles
          style={{
            height: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <AppText>Could't retrieve lists.</AppText>
          <View style={{width: 100}}>
            <AppButton title="Retry" onPress={() => getListngsData()} />
          </View>
        </View>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 15,
    backgroundColor: colors.lightGrey,
    flex: 1,
  },
});
