import React, {useState} from 'react';
import {FlatList} from 'react-native';
import {DeleteActionItem, ItemSeperator} from '../components/ListItem';
import ListItem from '../components/ListItem';

export default function MessageScreen(_props: any) {
  const [refreshing] = useState(false);
  const [messages, setMessages] = useState([
    {
      id: 1,
      title: 'prince kumar',
      subTitle: 'this prince kumar who are good to see you.',
      image: require('../assets/prince.jpg'),
    },
    {
      id: 2,
      title: 'prince kumar',
      subTitle: 'this prince kumar who are good to see you.',
      image: require('../assets/prince.jpg'),
    },
  ]);
  const handleDelete = (deleteMessage: any) => {
    setMessages(messages.filter(message => message.id !== deleteMessage.id));
  };
  return (
    <FlatList
      data={messages}
      renderItem={({item}) => (
        <ListItem
          title={item.title}
          subTitle={item.subTitle}
          image={item.image}
          onPress={() => console.log('item selected', item)}
          renderRightActions={() => (
            <DeleteActionItem onPress={() => handleDelete(item)} />
          )}
        />
      )}
      refreshing={refreshing}
      onRefresh={() => {
        setMessages([
          {
            id: 2,
            title: 'T2',
            subTitle: 'D2',
            image: require('../assets/prince.jpg'),
          },
          {
            id: 1,
            title: 'T1',
            subTitle: 'D1',
            image: require('../assets/prince.jpg'),
          },
          {
            id: 2,
            title: 'T2',
            subTitle: 'D2',
            image: require('../assets/prince.jpg'),
          },
        ]);
      }}
      ItemSeparatorComponent={ItemSeperator}
      keyExtractor={message => message.id.toString()}
    />
  );
}
