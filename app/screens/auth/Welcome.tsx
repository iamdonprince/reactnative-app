import React, {useContext} from 'react';
import {Image, View, Text, ImageBackground, StyleSheet} from 'react-native';
import AppButton from '../../components/AppButton';
import {AuthContext} from '../../components/AuthProviders';
import routes from '../../navigator/routes';

export default function Welcome({navigation}: any) {
  const {setIsLoginedIn} = useContext(AuthContext);
  return (
    <View style={styles.container}>
      <ImageBackground
        resizeMode="contain"
        style={styles.ImageBackground}
        source={require('../../assets/Saly.png')}>
        <View style={styles.logoContainer}>
          <Image
            resizeMode="contain"
            style={styles.logo}
            source={require('../../assets/logo-red.png')}
          />
          <Text style={styles.tagline}>Sell what you don't need.</Text>
        </View>
        <View style={styles.buttons}>
          <AppButton
            title="login"
            color="white"
            backgroundColor="primary"
            onPress={() => navigation.navigate(routes.LOGIN)}
          />
          <AppButton
            onPress={() => navigation.navigate(routes.REGISTER)}
            title="Register"
            color="white"
            backgroundColor="secondary"
          />

          <AppButton
            onPress={() => setIsLoginedIn(true)}
            title="Skip"
            color="white"
            backgroundColor="grey"
          />
        </View>
      </ImageBackground>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
  },
  ImageBackground: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  logo: {
    width: '100%',
    height: '100%',
  },
  logoContainer: {
    width: '100%',
    height: '10%',
    marginTop: 50,
    alignItems: 'center',
  },
  tagline: {
    fontSize: 18,
    fontWeight: '600',
    paddingVertical: 10,
  },
  buttons: {
    padding: 20,
    width: '100%',
  },
});
