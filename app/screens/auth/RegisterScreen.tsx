import React, {useContext} from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {Image, StyleSheet, View} from 'react-native';
import {AuthContext} from '../../components/AuthProviders';
import * as yup from 'yup';
import {SubmitButton, FormField} from '../../components/form';
import AppForm from '../../components/form/AppForm';
import routes from '../../navigator/routes';

const validationSchema = yup.object({
  userName: yup.string().required().label('UserName'),
  email: yup.string().required().email().label('Email'),
  password: yup.string().required().min(5).label('Password'),
});

export default function LoginScreen({navigation}: any) {
  const {register} = useContext(AuthContext);

  return (
    <View style={styles.container}>
      <View>
        <Image
          style={styles.logoImage}
          source={require('../../assets/logo-red.png')}
        />
      </View>
      <View style={styles.formContainer}>
        <AppForm
          initialValues={{email: '', password: ''}}
          onSubmit={({email, password, userName}: any) => {
            register({email, userName, password});
            navigation.navigate(routes.REGISTER);
          }}
          validationSchema={validationSchema}>
          <>
            <FormField
              name="userName"
              type="username"
              IconComponent={<Ionicons name="person" color="grey" size={20} />}
              placeholder="name"
            />
            <FormField
              name="email"
              type="emailAddress"
              IconComponent={<Ionicons name="mail" color="grey" size={20} />}
              placeholder="example@gmail.com"
            />

            <FormField
              name="password"
              type="password"
              secureTextEntry={true}
              IconComponent={
                <Ionicons name="lock-closed" color="grey" size={20} />
              }
              placeholder="password"
            />

            <SubmitButton title="Register" />
          </>
        </AppForm>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 30,
  },

  logoImage: {
    width: 90,
    height: 90,
    marginTop: 10,
  },
  formContainer: {
    width: '100%',
    marginTop: 20,
  },
});
