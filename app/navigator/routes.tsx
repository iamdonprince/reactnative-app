export default Object.freeze({
  LISTING_EDIT: 'ListingEdit',
  ACCOUNT: 'Account',
  LISTING_DETAILS: 'ListingDetails',
  MESSAGE: 'Message',
  LISTINGS: 'Listings',
  LOGIN: 'Login',
  REGISTER: 'Register',
  CAMERA: 'Camera',
  Welcome: 'Welcome',
});
