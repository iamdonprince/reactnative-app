import React from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import colors from '../config/colors';

export default function ListingBtn({onPress}: any) {
  return (
    <TouchableOpacity onPress={onPress} style={styles.container}>
      <MaterialCommunityIcons
        name="plus-circle"
        size={30}
        color={colors.white}
      />
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    width: 70,
    height: 70,
    backgroundColor: colors.primary,
    borderRadius: 35,
    bottom: 20,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 10,
    borderColor: colors.white,
  },
});
