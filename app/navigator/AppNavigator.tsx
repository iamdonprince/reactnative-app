import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
// import colors from '../config/colors';
import Ionicons from 'react-native-vector-icons/Ionicons';
import HomeNavigator from './HomeNavigator';
import AccountNavigator from './AccountNavigator';
import ListingEditScreen from '../screens/ListingEditScreen';
import ListingBtn from './ListingBtn';
import routes from './routes';

const Tab = createBottomTabNavigator();

export default function AppNavigator() {
  return (
    <Tab.Navigator
      screenOptions={({route}) => ({
        tabBarIcon: ({focused, color, size}) => {
          let iconName = '';

          if (route.name === 'Home') {
            iconName = focused ? 'home' : 'home-outline';
          } else if (route.name === 'Account') {
            iconName = focused ? 'person' : 'person-outline';
          } else if (route.name === 'Message') {
            iconName = focused
              ? 'chatbubble-ellipses'
              : 'chatbubble-ellipses-outline';
          } else if (route.name === 'Camera') {
            iconName = focused ? 'camera' : 'camera-outline';
          }

          // You can return any component that you like here!
          return <Ionicons name={iconName} size={size} color={color} />;
        },
      })}>
      <Tab.Screen name="Home" component={HomeNavigator} />
      <Tab.Screen
        name="ListingEdit"
        component={ListingEditScreen}
        options={({navigation}: any) => ({
          tabBarButton: () => (
            <ListingBtn
              onPress={() => navigation.navigate(routes.LISTING_EDIT)}
            />
          ),
        })}
      />

      <Tab.Screen name="Account" component={AccountNavigator} />
      {/* <Tab.Screen name="Message" component={MessageScreen} /> */}

      {/* <Tab.Screen name="Camera" component={ReactCamera} /> */}
    </Tab.Navigator>
  );
}
