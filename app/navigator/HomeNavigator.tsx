import React from 'react';
import Home from '../screens/Home';
import {createStackNavigator} from '@react-navigation/stack';
import ListingDetailsScreen from '../screens/ListingDetailsScreen';

const Stack = createStackNavigator();

export default function HomeNavigator() {
  return (
    <Stack.Navigator
      initialRouteName="Listings"
      mode="modal"
      screenOptions={{headerShown: false}}>
      <Stack.Screen name="Listings" component={Home} />
      <Stack.Screen
        name="ListingDetails"
        options={{title: 'Listing Details'}}
        component={ListingDetailsScreen}
      />
    </Stack.Navigator>
  );
}
