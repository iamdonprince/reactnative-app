import React, {useState} from 'react';
import {
  View,
  StyleSheet,
  Alert,
  Image,
  TouchableWithoutFeedback,
  ScrollView,
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import colors from '../config/colors';

interface ImagePicker {
  deleteHandler: Function;
  pickImageHandler: Function;
  images: string[];
}

export default function ImagePicker({
  deleteHandler,
  pickImageHandler,
  images,
}: ImagePicker) {
  return (
    <View style={styles.container}>
      {images.length < 5 && (
        <View style={styles.uploadImage}>
          <TouchableWithoutFeedback onPress={() => pickImageHandler()}>
            <Ionicons name="camera" size={40} color="grey" />
          </TouchableWithoutFeedback>
        </View>
      )}
      <ScrollView horizontal>
        {images &&
          images.map(image => (
            <View style={styles.image} key={image}>
              <TouchableWithoutFeedback onPress={() => deleteHandler(image)}>
                <Image
                  style={{width: '100%', height: '100%'}}
                  source={{uri: image}}
                />
              </TouchableWithoutFeedback>
            </View>
          ))}
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {flexDirection: 'row', width: '100%'},
  uploadImage: {
    width: 100,
    height: 100,
    backgroundColor: colors.white,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
  image: {
    width: 100,
    height: 100,
    backgroundColor: colors.white,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    marginHorizontal: 10,
    overflow: 'hidden',
  },
});
