import React from 'react';
import {Formik} from 'formik';

export default function AppForm({
  initialValues,
  validationSchema,
  onSubmit,
  children,
}: {
  initialValues: any;
  validationSchema: any;
  onSubmit: any;
  children: React.ReactNode;
}) {
  return (
    <Formik
      initialValues={initialValues}
      onSubmit={onSubmit}
      validationSchema={validationSchema}>
      {children}
    </Formik>
  );
}
