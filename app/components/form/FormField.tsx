import React from 'react';
import {TextInputIOSProps} from 'react-native';
import {useFormikContext} from 'formik';
import AppTextInput from '../TextInput';
import ErrorMessage from './ErrorMessage';

interface IFormField {
  type?: TextInputIOSProps['textContentType'];
  IconComponent?: Element;
  placeholder: string;
  name: string;
  secureTextEntry?: boolean;
  keyboardType?: any;
  width?: string | number;
}
export default function FormField({
  name,
  IconComponent,
  type,
  placeholder,
  secureTextEntry = false,
  keyboardType,
  width,
}: IFormField) {
  const {handleChange, errors, setFieldTouched, touched} = useFormikContext();
  return (
    <>
      <AppTextInput
        width={width}
        onChange={handleChange(name)}
        onBlur={() => setFieldTouched(name)}
        type={type}
        IconComponent={IconComponent}
        placeholder={placeholder}
        secureTextEntry={secureTextEntry}
        keyboardType={keyboardType}
      />
      <ErrorMessage error={errors} visible={touched} name={name} />
    </>
  );
}
