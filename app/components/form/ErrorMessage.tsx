import React from 'react';
import AppText from '../AppText';

export default function ErrorMessage({
  error,
  visible,
  name,
}: {
  error: any;
  visible: any;
  name: any;
}) {
  if (!visible || !error) {
    return null;
  }
  return <AppText style={{color: 'red'}}>{error[name]}</AppText>;
}
