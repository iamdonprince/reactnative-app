import React from 'react';
import {View, Text, Alert} from 'react-native';
import {useFormikContext} from 'formik';
import {launchImageLibrary} from 'react-native-image-picker';
import ImagePicker from '../ImagePicker';
import ErrorMessage from './ErrorMessage';

export default function ImageFormInput({name}: {name: string}) {
  const {errors, values, setFieldValue, touched} = useFormikContext();
   const pickImage = () => {
    launchImageLibrary({mediaType: 'photo', quality: 0.5}, response => {
      if (response.didCancel) {
        console.log('User cancelled photo picker');
        Alert.alert('You did not select any image');
      } else if (response.errorMessage) {
        console.log('ImagePicker Error: ', response.errorMessage);
        Alert.alert(response.errorMessage);
      } else {
        let source = {uri: response.uri};
        console.log({source});
        setFieldValue(name, [...values[name], source.uri]);
      }
    });
  };
  const deleteHandler = image => {
    Alert.alert('Delete', 'Are you sure you want to delete this image?', [
      {
        text: 'Yes',
        onPress: () =>
          setFieldValue(
            name,
            values[name].filter(selected => selected !== image),
          ),
      },
      {text: 'No'},
    ]);
  };
  return (
    <>
      <ImagePicker
        images={values[name]}
        deleteHandler={deleteHandler}
        pickImageHandler={pickImage}
      />
      <ErrorMessage error={errors} name={name} visible={touched} />
    </>
  );
}
