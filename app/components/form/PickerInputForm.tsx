import {useFormikContext} from 'formik';
import React from 'react';
import {Category} from '../../@types/Form';
import AppPicker from '../AppPicker';

export default function PickerInputForm({
  items,
  name,
  placeholder,
  width,
}: {
  items: Category[];
  name: string;
  placeholder: string;
  width?: string | number;
}) {
  const {values, setFieldValue} = useFormikContext();

  return (
    <AppPicker
      items={items}
      width={width}
      placeholder={placeholder}
      label={values[name]}
      onPress={(value: string) => setFieldValue(name, value)}
    />
  );
}
