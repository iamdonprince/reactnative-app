import React from 'react';
import LottieView from 'lottie-react-native';

const ActivityIndicator = ({isVisible}: {isVisible: boolean}) => {
  if (!isVisible) return null;

  return (
    <LottieView
      source={require('../assets/animations/loader.json')}
      autoPlay
      loop
      style={{padding:200,backgroundColor:"#fff"}}
    />
  );
};

export default ActivityIndicator;
