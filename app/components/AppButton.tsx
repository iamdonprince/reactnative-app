import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import Colors from '../config/colors';
interface ButtonType {
  color?: keyof typeof Colors;
  title?: string;
  backgroundColor?: keyof typeof Colors;
  onPress?: any;
  style?: any;
  icon?: React.ReactNode;
}
export default function AppButton({
  color = 'white',
  title,
  backgroundColor = 'primary',
  onPress,
  icon,
  ...style
}: ButtonType) {
  const styles = StyleSheet.create({
    buttons: {
      backgroundColor: Colors[backgroundColor],
      padding: 15,
      justifyContent: 'center',
      alignItems: 'center',
      width: '100%',
      borderRadius: 25,
      marginVertical: 10,
      ...style,
    },
    text: {
      fontSize: 18,
      color: Colors[color],
      textTransform: 'capitalize',
      fontWeight: 'bold',
    },
  });
  return (
    <TouchableOpacity style={styles.buttons} onPress={onPress}>
      {icon}
      {title && <Text style={styles.text}>{title}</Text>}
    </TouchableOpacity>
  );
}
