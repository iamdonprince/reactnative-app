import React from 'react';
import {StyleSheet, View, Image} from 'react-native';
import {TouchableWithoutFeedback} from 'react-native-gesture-handler';
import {ICard} from '../@types/Listings';
import colors from '../config/colors';
import AppText from './AppText';

export default function Card({
  title,
  subTitle,
  imageUrl,
  onPress,
}: Omit<ICard, 'images'>) {
  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <View style={styles.container}>
        <Image style={styles.image} source={{uri: imageUrl}} />
        <View style={styles.detailsContainer}>
          <AppText style={styles.title}>{title}</AppText>
          <AppText style={styles.price}>{subTitle}</AppText>
          {/* <View style={styles.itemList}>
          <ListItem
            image={require("../assets/prince.jpg")}
            title="prince kumar"
            subTitle="5 testing"
          />
        </View> */}
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    borderRadius: 15,
    overflow: 'hidden',
    marginBottom: 15,
  },
  itemList: {
    marginVertical: 40,
  },
  image: {
    width: '100%',
    height: 200,
  },
  detailsContainer: {
    padding: 15,
    backgroundColor: colors.white,
  },
  title: {
    color: colors.black,
    fontSize: 24,
    fontWeight: '500',
  },
  price: {
    color: colors.secondary,
    fontWeight: 'bold',
    fontSize: 20,
    marginVertical: 8,
  },
});
