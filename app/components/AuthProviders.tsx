import React, {createContext, useState} from 'react';

interface IUserData {
  email: string;
  password: string;
  userName: string;
}
interface IAuthContext {
  userData: IUserData | null;
  setIsLoginedIn: any;
  register: (userData: {
    email: string;
    password: string;
    userName: string;
  }) => void;
  login: (userData: {email: string; password: string}) => void;
  logout: () => void;
  isLoginedIn: boolean;
}

export const AuthContext = createContext({} as IAuthContext);

interface AuthProvidersProps {}

export const AuthProviders: React.FC<AuthProvidersProps> = ({children}) => {
  const [userData, setData] = useState<IUserData | null>(null);
  const [isLoginedIn, setIsLoginedIn] = useState<boolean>(false);

  const register = ({email, password, userName}: IUserData) => {
    setData({email, password, userName});
  };
  const login = ({email, password}: Pick<IUserData, 'email' | 'password'>) => {
    if (userData?.email === email && userData?.password === password) {
      setIsLoginedIn(true);
    }
  };
  function logout() {
    setIsLoginedIn(false);
  }
  return (
    <AuthContext.Provider
      value={{userData, login, isLoginedIn, setIsLoginedIn, register, logout}}>
      {children}
    </AuthContext.Provider>
  );
};
