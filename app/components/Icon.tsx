import React from 'react';
import {StyleSheet, View} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialIcons';

interface IconType {
  size?: number;
  name: string;
  backgroundColor?: string;
  iconColor?: string;
}

export default function Icon({
  size = 40,
  name,
  backgroundColor,
  iconColor = 'white',
}: IconType) {
  const styles = StyleSheet.create({
    container: {
      backgroundColor,
      width: size,
      height: size,
      borderRadius: size / 2,
      justifyContent: 'center',
      alignItems: 'center',
    },
  });

  return (
    <View style={styles.container}>
      <MaterialCommunityIcons size={size * 0.5} name={name} color={iconColor} />
    </View>
  );
}
