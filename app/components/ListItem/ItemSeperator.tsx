import React from 'react';
import {StyleSheet, View} from 'react-native';
import colors from '../../config/colors';

export default function ItemSeperator() {
  const styles = StyleSheet.create({
    seperator: {
      width: '100%',
      height: 1,
      backgroundColor: colors.lightGrey,
    },
  });
  return <View style={styles.seperator} />;
}
