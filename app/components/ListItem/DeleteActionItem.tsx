import React from 'react';
import {StyleSheet, TouchableWithoutFeedback, View} from 'react-native';
import colors from '../../config/colors';
import Icon from '../Icon';
export default function DeleteActionItem({onPress}: any) {
  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <View style={styles.container}>
        <Icon size={35} name="delete-outline" iconColor="white" />
      </View>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.danger,
    width: 70,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
