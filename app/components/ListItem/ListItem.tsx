import React from 'react';
import {Image, StyleSheet, TouchableHighlight, View} from 'react-native';
import colors from '../../config/colors';
import AppText from '../AppText';
import {gestureHandlerRootHOC, Swipeable} from 'react-native-gesture-handler';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialIcons';

interface IList {
  image?: any;
  title: string;
  subTitle?: string;
  onPress?: (props: any) => void;
  renderRightActions?: any;
  IconComponent?: React.ReactNode;
}

export default gestureHandlerRootHOC(function ListItem({
  image,
  title,
  subTitle,
  onPress,
  renderRightActions,
  IconComponent,
}: IList) {
  return (
    <Swipeable renderRightActions={renderRightActions}>
      <TouchableHighlight underlayColor={colors.lightGrey} onPress={onPress}>
        <View style={styles.container}>
          {IconComponent}
          {image && (
            <Image resizeMode="cover" style={styles.image} source={image} />
          )}
          <View style={styles.detailsContainer}>
            <AppText numberOfLines={1} style={styles.title}>
              {title}
            </AppText>
            {subTitle && (
              <AppText numberOfLines={1} style={styles.subtitle}>
                {subTitle}
              </AppText>
            )}
          </View>
          <MaterialCommunityIcons
            size={25}
            color={colors.grey}
            name="chevron-right"
          />
        </View>
      </TouchableHighlight>
    </Swipeable>
  );
});

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    padding: 15,
    alignItems: 'center',
  },
  detailsContainer: {
    marginHorizontal: 10,
    justifyContent: 'center',
    flex: 1,
  },
  title: {
    fontSize: 18,
    fontWeight: '600',
    textTransform: 'capitalize',
  },
  subtitle: {
    color: colors.mediumGrey,
  },
  image: {
    width: 70,
    height: 70,
    borderRadius: 50,
  },
});
