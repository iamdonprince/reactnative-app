import React from 'react';
import {Text, StyleSheet, Platform} from 'react-native';

export default function AppText({
  children,
  style,
  numberOfLines,
  ...rest
}: {
  children: React.ReactNode;
  style?: any;
  rest?: any;
  numberOfLines?: number;
}) {
  const styles = StyleSheet.create({
    text: {
      fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Roboto',
      color: 'black',
      textTransform: 'capitalize',
    },
  });
  return (
    <Text numberOfLines={numberOfLines} {...rest} style={[styles.text, style]}>
      {children}
    </Text>
  );
}
