import React from 'react';
import {
  StyleSheet,
  TextInput,
  View,
  Platform,
  TextInputIOSProps,
} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import colors from '../config/colors';

interface IInputeType {
  icon?: string;
  placeholder?: string;
  IconComponent?: React.ReactNode;
  secureTextEntry?: boolean;
  type?: TextInputIOSProps['textContentType'];
  onChange: any;
  onBlur: () => void;
  keyboardType: any;
  width?: number | string;
}
export default function AppTextInput({
  icon,
  placeholder,
  IconComponent,
  secureTextEntry = false,
  type = 'none',
  onChange,
  keyboardType,
  width,
  onBlur,
}: IInputeType) {
  const styles = StyleSheet.create({
    container: {
      backgroundColor: colors.white,
      marginVertical: 10,
      padding: 5,
      flexDirection: 'row',
      borderRadius: 5,
      alignItems: 'center',
      width: width || '100%',
    },
    textInput: {
      fontFamily: Platform.OS === 'ios' ? 'Avenir' : 'Roboto',
      fontSize: 16,
      flex: 1,
      color: colors.dark,
    },
  });

  return (
    <View style={styles.container}>
      {IconComponent
        ? IconComponent
        : icon && (
            <MaterialCommunityIcons
              size={20}
              color={colors.mediumGrey}
              name={icon}
              style={{marginRight: 10}}
            />
          )}
      <TextInput
        keyboardType={keyboardType}
        onChangeText={onChange}
        textContentType={type}
        secureTextEntry={secureTextEntry}
        placeholder={placeholder}
        style={styles.textInput}
        onBlur={onBlur}
      />
    </View>
  );
}
