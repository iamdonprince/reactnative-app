import React, {useState} from 'react';
import {Modal, StyleSheet, TouchableWithoutFeedback, View} from 'react-native';
import {FlatList} from 'react-native-gesture-handler';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import colors from '../config/colors';
import AppText from './AppText';
import ListItem from './ListItem';
import AppButton from './AppButton';
export default function AppPicker({
  icon,
  placeholder,
  items,
  label,
  onPress,
  width,
}: {
  icon?: string;
  placeholder: string;
  items: any;
  label: string;
  width?: string | number;
  onPress: (velue: string) => void;
}) {
  const [visible, setVisible] = useState(false);
  const styles = StyleSheet.create({
    container: {
      width: width || '100%',
      backgroundColor: colors.white,
      marginVertical: 10,
      padding: 20,
      flexDirection: 'row',
      borderRadius: 10,
      alignItems: 'center',
    },
    text: {
      flex: 1,
      color: 'grey',
    },
    modalView: {
      margin: 20,
      backgroundColor: 'white',
      borderRadius: 20,
      height: '40%',
      padding: 35,
      alignItems: 'center',
      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 4,
      elevation: 5,
    },
  });
  return (
    <>
      <TouchableWithoutFeedback onPress={() => setVisible(!visible)}>
        <View style={styles.container}>
          {icon && (
            <MaterialCommunityIcons
              size={20}
              color={colors.grey}
              name={icon}
              style={{marginRight: 10}}
            />
          )}
          <AppText style={styles.text}>{label ? label : placeholder}</AppText>
          <MaterialCommunityIcons
            size={20}
            color={colors.grey}
            name="chevron-down"
          />
        </View>
      </TouchableWithoutFeedback>
      <Modal
        visible={visible}
        animationType="slide"
        transparent={false}
        onRequestClose={() => {
          setVisible(!visible);
        }}>
        <AppButton
          title="Close"
          color="brightBlue"
          backgroundColor="white"
          onPress={() => setVisible(!visible)}
        />
        <FlatList
          data={items}
          renderItem={({item}) => (
            <ListItem
              onPress={() => {
                onPress(item.label);
                setVisible(!visible);
              }}
              title={item.label}
            />
          )}
          keyExtractor={list => list.value}
        />
      </Modal>
    </>
  );
}
