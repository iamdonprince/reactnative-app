import {useEffect, useState} from 'react';
// import RNLocation from 'react-native-location';
import * as Location from 'expo-location';
export default function useLocation() {
  // RNLocation.configure({
  //   distanceFilter: 0,
  // });
  const [location, setLocation] = useState<{
    latitude: number;
    longitude: number;
  }>();
  const [errorMsg, setErrorMsg] = useState<string>('');

  useEffect(() => {
    (async () => {
      let {status} = await Location.requestForegroundPermissionsAsync();
      if (status !== 'granted') {
        setErrorMsg('Permission to access location was denied');
        return;
      }

      let {
        coords: {latitude, longitude},
      } = await Location.getCurrentPositionAsync({});
      setLocation({latitude, longitude});
    })();
  }, []);

  if (errorMsg) {
    return errorMsg;
  }
  return location;
  //const getLocation = async () => {
  //   const granted = await RNLocation.requestPermission({
  //     ios: 'whenInUse', // or 'always'
  //     android: {
  //       detail: 'coarse', // or 'fine'
  //       rationale: {
  //         title: 'We need to access your location',
  //         message: 'We use your location to show where you are on the map',
  //         buttonPositive: 'OK',
  //         buttonNegative: 'Cancel',
  //       },
  //     },
  //   });

  //   if (granted) {
  //     //   let location = await RNLocation.getLatestLocation({
  //     //     timeout: 1000,
  //     //   });
  //     //   console.log(
  //     //     location,
  //     //     location?.longitude,
  //     //     location?.latitude,
  //     //     location?.timestamp,
  //     //   );

  //     let locationSubscription = RNLocation.subscribeToLocationUpdates(
  //       location => {
  //         /* Example location returned
  //         {
  //           speed: -1,
  //           longitude: -0.1337,
  //           latitude: 51.50998,
  //           accuracy: 5,
  //           heading: -1,
  //           altitude: 0,
  //           altitudeAccuracy: -1
  //           floor: 0
  //           timestamp: 1446007304457.029,
  //           fromMockProvider: false
  //         }
  //         */
  //         if (location) {
  //           setLocation({
  //             lat: location[0].altitude,
  //             lng: location[0].longitude,
  //           });
  //         }
  //       },
  //     );
  //   }
  // };
  // getLocation();
}
