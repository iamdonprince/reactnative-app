const endpoint = 'http://192.168.43.28:9000/api/listings';

const getListings = async () => {
  const res = await fetch(endpoint);
  const data = await res.json();
  return data;
};

export default {
  getListings,
};
